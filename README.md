# Computer Graphics Lab 2019

This repository is a modified seed/start for the Computer Graphics course Johannes Kepler University Linz, Austria. For further details concerning organization, have a look at the original README file `README_JKU.md`.

This repository contains the finished version of the first lab (subfolder `01_intro` in the original project).

# Features

This main features of this seed are:
  * Main source code in Typescript, augmenting bigger projects (better readability, detect type problems at compile time, etc.)
  * Type definitions ("Typings") for provided libraries (gl-matrix@2.3.2 and custom CG framework)
  * Modern ES2015 language level and bundling for clean and readable source code
  * Bundling of deliverable bundle.js file via Webpack (de-facto industry standard, performs numerous optimizations, output in ES5 compatible with a vast range of browsers/versions)
  * Dependency management based on Node Package Manager (npm), which is turn is based on NodeJS (de-facto industry standards)

# Installation

Ensure you have a recent version of NodeJS (preferably >= 8, check via `node --version`) and npm (preferably >= 6, check via `npm --version`) installed. If not installed, look at the respective websites for installation instructions.

Once these tools are available, simply run `npm install` to install all required dependencies (as specified in `package.json`).

# Build Toolchain/Deployment

Due to requirements for the CG project there are 2 separate build steps in use.

1. Typescript Compiler `tsc`: Converts the original Typescript source code into ES2015 Javascript code. Output resides in `disct/tsc/es2015`. The output maintains the original folder structure and preserves whitespaces and comments (only type informations is removed). This code can be represented during the Code review session.
2. Webpack Bundler `webpack`: Ingests ES2015 code from `disct/tsc/es2015` and performs the usual Webpack bundling process. Thus all JS files are minimized, transpiled to ES5 (for maximum compatibility) and combined to a single `bundle.umd.js` file.

Similar to the libraries `gl-matrix.js` and the CG `framework.js` the finished `bundle.umd.js` is included as a script tag into the HTML page (`index.html`). `index.html` is entirely static, deployment may be done either via
* Open `index.html` directly in a browser or…
* Start a Webserver of your choice to host `index.html` entry file and dependencies.

## Single build

To invoke a single compilations process, run `npm run build`.

## Continuous build (watch mode)

If want to a continuous build process (incremental compilation on each file source file save), run the following 2 commands in parallel (i.e. in separate terminals):
* `npm run start-tsc`
* `npm run start-webpack`

# Recommended Tools

Here are some recommendations for a good developer experience:
* VS Code: IDE for WebDev and native Typescript support. Extension recommendations:
  * vscode-icons
  * EditorConfig for VS Code
  * Document This
  * Live Server
  * Shader language support for VS Code
