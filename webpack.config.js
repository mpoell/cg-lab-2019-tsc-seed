const path = require('path');

module.exports = {
  entry: './dist/tsc/es2015/main.js',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'webpack/es5/bundle.umd.js',
    libraryTarget: 'umd'
  },
  externals: { // Previously loaded libraries, both are registered in the global window scope
    'gl-matrix': {
      root: ['window'],
      commonjs2: 'gl-matrix',
      commonjs: 'gl-matrix',
      amd: 'gl-matrix'
    },
    'cg-lab-framework': {
      root: ['window'],
      commonjs2: 'cg-lab-framework',
      commonjs: 'cg-lab-framework',
      amd: 'cg-lab-framework'
    },
  },
};
