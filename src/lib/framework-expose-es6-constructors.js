// ES2015 class constructors are not visible in window per default
// This poses a probelm when using Webpack externals, thus expose them explicitly

window.SGNode = SGNode;
window.ShaderSGNode = ShaderSGNode;
window.TransformationSGNode = TransformationSGNode;
window.SetUniformSGNode = SetUniformSGNode;
window.AdvancedTextureSGNode = AdvancedTextureSGNode;
window.RenderSGNode = RenderSGNode;
window.MaterialSGNode = MaterialSGNode;
window.LightSGNode = LightSGNode;
