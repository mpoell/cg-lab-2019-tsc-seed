// Typings for JKU CG framework by Manuel Pöll
declare module 'cg-lab-framework' {
  import { vec3, vec4, mat4 } from "gl-matrix";

  ////////////////////////////////////////////////////////////////////////////////
  /* Simple Custom Types                                                        */

  /**
   * Most of the sample code (including the framework these typings describe)
   * uses untyped {@link Array}s interchangeable with typed {@link Float32Array}s. Furthermore MDN
   * (see e.g. https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/uniform#Parameters)
   * presents untyped arrays of numbers as equally valid WebGL parameters (every browser I've
   * tested does support unteyped {@link Array}s).
   *
   * However, technically the WebGL spec only permits typed {@link Float32Array}s. Since the
   * framework itself does not abide by this, these typings reflect that.
   */
  type vec3Untyped = [number, number, number];
  type vec4Untyped = [number, number, number, number];
  type mat4Untyped = [
    number, number, number, number,
    number, number, number, number,
    number, number, number, number,
    number, number, number, number
  ];

  ////////////////////////////////////////////////////////////////////////////////
  /* Resources                                                                  */

  /**
   * Map of strings for the resource paths
   *
   * @export
   * @interface ResourcePaths
   */
  export interface ResourcePaths {
    [key: string]: string;
  }
  /**
   * Mapped type that resolves a existing {@link ResourcePaths}
   */
  type ResolveResources<T> = {
    [P in keyof T]: any;
  }

  /**
   * load resources identified by a key: url store returning a promise when everything is loaded
   *
   * @export
   * @template T exact type of the resources
   * @param {T} resources the resources to load
   * @param {(resources: T) => void} [callback] optional callback as an alternative to the promise
   * @returns {Promise<T>}
   */
  export function loadResources<T extends ResourcePaths>(
    resources: T,
    callback?: (resources: ResolveResources<T>) => void
  ): Promise<ResolveResources<T>>;
  ////////////////////////////////////////////////////////////////////////////////
  /* Text display over WebGL canvas                                             */

  /**
   * creates a html-paragraph for displaying text on screen
   *
   * @param {HTMLCanvasElement} canvas the canvas the paragraph will be placed upon
   */
  export function createHtmlText(canvas: HTMLCanvasElement): void;
  /**
   * displays the text given as a parameter on the screen
   *
   * @param {string} text the text we want to show
   */
  export function displayText(text: string): void;
  /**
   * clears the text on the screen
   */
  export function clearText(): void;
  ////////////////////////////////////////////////////////////////////////////////
  /* Wrapper for various creation functions                                     */

  /**
   * creates a shader of the given type with the given code
   *
   * @param {WebGLRenderingContext} gl GL context
   * @param {string} code the shader code
   * @param {('vertex' | 'fragment')} type shader type
   * @returns {(WebGLShader | null)}
   */
  export function createShader(
    gl: WebGLRenderingContext,
    code: string,
    type: 'vertex' | 'fragment'
  ): WebGLShader | null;
  /**
   * creates a program by the given vertex and fragment shader
   *
   * @param {WebGLRenderingContext} gl GL context
   * @param {(string | WebGLShader)} vertex vertex shader or code
   * @param {(string | WebGLShader)} fragemt fragment shader or code
   * @returns {(WebGLRenderingContext | null)}
   */
  export function createProgram(
    gl: WebGLRenderingContext,
    vertex: string | WebGLShader,
    fragemt: string | WebGLShader
  ): WebGLRenderingContext | null;
  /**
   * creates a WebGLRenderingContext along with a canvas to render to
   *
   * @param {number} width
   * @param {number} height
   * @returns {(WebGLRenderingContext | null)}
   */
  export function createContext(
    width?: number,
    height?: number
  ): WebGLRenderingContext | null;


  ////////////////////////////////////////////////////////////////////////////////
  /* Miscellaneous                                                              */

  /**
   * checks and updates the canvas size according to its current real size
   *
   * @param {WebGLRenderingContext} gl
   */
  export function checkForWindowResize(
    gl: WebGLRenderingContext
  ): void;
  /**
   * checks whether the given attribute location is valid
   *
   * @param {number} loc
   * @returns {boolean}
   */
  export function isValidAttributeLocation(
    loc: number
  ): boolean;
  /**
   * checks whether the given uniform location is valid
   *
   * @param {WebGLUniformLocation} loc
   * @returns {boolean}
   */
  export function isValidUniformLocation(
    loc: WebGLUniformLocation
  ): boolean;
  ////////////////////////////////////////////////////////////////////////////////
  /* Conversion                                                                 */

  export interface RGBATuple {
    r: number;
    g: number;
    b: number;
    a: number;
  }
  /**
   * converts the given hex color, e.g., #FF00FF to an rgba tuple
   *
   * @param {string} color
   * @returns {RGBATuple}
   */
  export function hex2rgba(
    color: string
  ): RGBATuple;
  ////////////////////////////////////////////////////////////////////////////////
  /* Math                                                                       */

  export interface TransformSettings {
    translate?: vec3 | vec3Untyped;
    rotateX?: number;
    rotateY?: number;
    rotateZ?: number;
    scale?: number;
  }
  export interface GLMTools {
    deg2rad: (degrees: number) => number;
    translate: (x: number, y: number, z: number) => mat4;
    scale: (x: number, y: number, z: number) => mat4;
    rotateX: (degree: number) => mat4;
    rotateY: (degree: number) => mat4;
    rotateZ: (degree: number) => mat4;
    transform: (transform: TransformSettings) => mat4;
  }
  export var glm: GLMTools;
  ////////////////////////////////////////////////////////////////////////////////
  /* Models                                                                     */

  export interface Model {
    position: number[];
    normal: number[];
    texture: number[];
    index: number[];
  }
  /**
   * returns the model of a sphere with the given radius
   *
   * @param {number} [radius]
   * @param {number} [latitudeBands]
   * @param {number} [longitudeBands]
   * @returns {Model}
   */
  export function makeSphere(
    radius?: number,
    latitudeBands?: number,
    longitudeBands?: number
  ): Model;
  /**
   * returns the model of a new rect of the given width and height
   *
   * @param {number} [width]
   * @param {number} [height]
   * @returns {Model}
   */
  export function makeRect(
    width?: number,
    height?: number
  ): Model;
  /**
   * Based on https://github.com/frenchtoast747/webgl-obj-loader/blob/master/webgl-obj-loader.js
   *
   * The OBJ file format does a sort of compression when saving a model in a
   * program like Blender. There are at least 3 sections (4 including textures)
   * within the file. Each line in a section begins with the same string:
   *   * 'v': indicates vertex section
   *   * 'vn': indicates vertex normal section
   *   * 'f': indicates the faces section
   *   * 'vt': indicates vertex texture section (if textures were used on the model)
   * Each of the above sections (except for the faces section) is a list/set of
   * unique vertices.
   * Each line of the faces section contains a list of
   * (vertex, [texture], normal) groups
   * Some examples:
   *     // the texture index is optional, both formats are possible for models
   *     // without a texture applied
   *     f 1/25 18/46 12/31
   *     f 1//25 18//46 12//31
   *     // A 3 vertex face with texture indices
   *     f 16/92/11 14/101/22 1/69/1
   *     // A 4 vertex face
   *     f 16/92/11 40/109/40 38/114/38 14/101/22
   * The first two lines are examples of a 3 vertex face without a texture applied.
   * The second is an example of a 3 vertex face with a texture applied.
   * The third is an example of a 4 vertex face. Note: a face can contain N
   * number of vertices.
   * Each number that appears in one of the groups is a 1-based index
   * corresponding to an item from the other sections (meaning that indexing
   * starts at one and *not* zero).
   * For example:
   *     `f 16/92/11` is saying to
   *       - take the 16th element from the [v] vertex array
   *       - take the 92nd element from the [vt] texture array
   *       - take the 11th element from the [vn] normal array
   *     and together they make a unique vertex.
   * Using all 3+ unique Vertices from the face line will produce a polygon.
   * Now, you could just go through the OBJ file and create a new vertex for
   * each face line and WebGL will draw what appears to be the same model.
   * However, vertices will be overlapped and duplicated all over the place.
   * Consider a cube in 3D space centered about the origin and each side is
   * 2 units long. The front face (with the positive Z-axis pointing towards
   * you) would have a Top Right vertex (looking orthogonal to its normal)
   * mapped at (1,1,1) The right face would have a Top Left vertex (looking
   * orthogonal to its normal) at (1,1,1) and the top face would have a Bottom
   * Right vertex (looking orthogonal to its normal) at (1,1,1). Each face
   * has a vertex at the same coordinates, however, three distinct vertices
   * will be drawn at the same spot.
   * To solve the issue of duplicate Vertices (the `(vertex, [texture], normal)`
   * groups), while iterating through the face lines, when a group is encountered
   * the whole group string ('16/92/11') is checked to see if it exists in the
   * packed.hashindices object, and if it doesn't, the indices it specifies
   * are used to look up each attribute in the corresponding attribute arrays
   * already created. The values are then copied to the corresponding unpacked
   * array (flattened to play nice with WebGL's ELEMENT_ARRAY_BUFFER indexing),
   * the group string is added to the hashindices set and the current unpacked
   * index is used as this hashindices value so that the group of elements can
   * be reused. The unpacked index is incremented. If the group string already
   * exists in the hashindices object, its corresponding value is the index of
   * that group and is appended to the unpacked indices array.
   *
   * @param {string} objectData
   * @returns {Model}
   */
  export function parseObjFile(
    objectData: string
  ): Model;
  ////////////////////////////////////////////////////////////////////////////////
  /* Materials                                                                  */

  export interface Material {
    ambient: vec4 | vec4Untyped;
    diffuse: vec4 | vec4Untyped;
    specular: vec4 | vec4Untyped;
    emission: vec4 | vec4Untyped;
    shininess: number;
    texture: string | null;
  }
  /**
   * Presumably http://paulbourke.net/dataformats/mtl/
   *
   * @param {string} fileContent
   * @returns {Material}
   */
  export function parseMtlFile(
    fileContent: string
  ): Material;

  ////////////////////////////////////////////////////////////////////////////////
  /* Scene Graph Nodes                                                          */

  export interface SGContext {
    gl: WebGLRenderingContext;
    sceneMatrix: mat4 | mat4Untyped;
    viewMatrix: mat4 | mat4Untyped;
    projectionMatrix: mat4 | mat4Untyped;
    shader: WebGLProgram;
  }

  /**
   * returns a new rendering context
   *
   * @export
   * @param {WebGLRenderingContext} gl the gl context
   * @param {(mat4 | mat4Untyped)} [projectionMatrix] optional projection Matrix
   * @returns {SGContext}
   */
  export function createSGContext(
    gl: WebGLRenderingContext,
    projectionMatrix?: mat4 | mat4Untyped
  ): SGContext;

  type ModelRendererFunction = (context: SGContext) => void;
  /**
   * a factory method for creating a model renderer
   *
   * @param {Model} model the model to render
   * @returns {ModelRendererFunction}
   */
  export function modelRenderer(model: Model): ModelRendererFunction;

  type SGNodeSingleOrMultiple = SGNode | SGNode[];

  /**
   * base node of the scenegraph
   *
   * @class SGNode
   */
  export class SGNode {
    children: SGNode[];

    /**
     * new bas node of a scenegraph
     *
     * @param {(SGNode | SGNode[])} [children] optional list of children or a single child to add
     * @memberof SGNode
     */
    constructor(children?: SGNode | SGNode[]);

    /**
     * appends a new child to this node
     *
     * @param {SGNode} child the child to append
     * @returns {SGNode} the child
     * @memberof SGNode
     */
    append(child: SGNode): SGNode;
    /**
     * alias to append
     *
     * @param {SGNode} child
     * @returns {SGNode}
     * @memberof SGNode
     */
    push(child: SGNode): SGNode;
    /**
     * removes a child from this node
     *
     * @param {SGNode} child
     * @returns {boolean} whether the operation was successful
     * @memberof SGNode
     */
    remove(child: SGNode): boolean;
    /**
     * render method to render this scengraph
     *
     * @param {SGContext} context
     * @memberof SGNode
     */
    render(context: SGContext): void;
  }

  /**
   * a transformation node, i.e applied a transformation matrix to its successors
   *
   * @class TransformationSGNode
   * @extends {SGNode}
   */
  export class TransformationSGNode extends SGNode {
    matrix: mat4 | mat4Untyped;

    /**
     * @param {(mat4 | mat4Untyped)} [matrix] the matrix to apply
     * @param {SGNodeSingleOrMultiple} [children] optional children
     * @memberof TransformationSGNode
     */
    constructor(matrix?: mat4 | mat4Untyped, children?: SGNodeSingleOrMultiple);
  }

  /**
   * a shader node sets a specific shader for the successors
   *
   * @class ShaderSGNode
   * @extends {SGNode}
   */
  export class ShaderSGNode extends SGNode {
    program: WebGLProgram;

    /**
     * constructs a new shader node with the given program
     *
     * @param {WebGLProgram} program the shader program to use
     * @param {SGNodeSingleOrMultiple} [children] optional list of children
     * @memberof ShaderSGNode
     */
    constructor(program: WebGLProgram, children?: SGNodeSingleOrMultiple);
  }

  type UniformValue =
    number // gl.uniform1f
    | boolean // gl.uniform1i
    | number[] // gl.uniformxf for x elements
  ;
  interface UniformMap {
    [key: string]: UniformValue;
  }
  /**
   * a utility node for setting a uniform in a shader
   *
   * @class SetUniformSGNode
   * @extends {SGNode}
   */
  export class SetUniformSGNode extends SGNode {
    uniforms: UniformMap;

    constructor(children?: SGNode | SGNode[]);
    constructor(uniform: string, value: UniformValue, children?: SGNodeSingleOrMultiple);

    setUniforms(context: SGContext): void;
  }

  export class AdvancedTextureSGNode extends SGNode {
    image: TexImageSource;
    textureunit: number;
    uniform: string;
    textureId: number;

    constructor(image: TexImageSource, children?: SGNodeSingleOrMultiple);

    init(gl: WebGLRenderingContext): void;
  }

  /**
   * a render node renders a specific model
   *
   * @class RenderSGNode
   * @extends {SGNode}
   */
  export class RenderSGNode extends SGNode {
    renderer: ModelRendererFunction;

    constructor(renderer: ModelRendererFunction | Model, children?: SGNodeSingleOrMultiple);

    setTransformationUniforms(context: SGContext): void;
  }

  /**
   * a material node represents one material including (ambient, diffuse, specular, emission, and shininess)
   *
   * @class MaterialSGNode
   * @extends {SGNode}
   */
  export class MaterialSGNode extends SGNode {
    ambient: vec4 | vec4Untyped;
    diffuse: vec4 | vec4Untyped;
    specular: vec4 | vec4Untyped;
    emission: vec4 | vec4Untyped;
    shininess: number;
    uniform: string;
    lights: any[];

    constructor(children: SGNodeSingleOrMultiple);

    setMaterialUniforms(context: SGContext): void;
  }

  /**
   * a light node represents a light including light position and light properties (ambient, diffuse, specular)
   * the light position will be transformed according to the current model view matrix
   *
   * @class LightSGNode
   * @extends {SGNode}
   */
  export class LightSGNode extends SGNode {
    position: vec3 | vec3Untyped;
    ambient: vec4 | vec4Untyped;
    diffuse: vec4 | vec4Untyped;
    specular: vec4 | vec4Untyped;
    uniform: string;
    _worldPosition: null | (vec3 | vec3Untyped);

    constructor(position: vec3 | vec3Untyped, children?: SGNodeSingleOrMultiple);

    setLightUniforms(context: SGContext): void;
    setLightPosition(context: SGContext): void;
    computeLightPosition(context: SGContext): void;
    /**
     * set the light uniforms without updating the last light position
     *
     * @param {SGContext} context
     * @memberof LightSGNode
     */
    setLight(context: SGContext): void;
  }

  /**
   * factory utils, WIP
   *
   * @interface SGUtils
   */
  export interface SGUtils {
    root: (...args: any[]) => SGNode;
    transform: (matrix: any, ...args: any[]) => TransformationSGNode;
    translate: (x: any, y: any, z: any, ...args: any[]) => any;
    scale: (x: any, y: any, z: any, ...args: any[]) => any;
    rotateX: (degree: any, ...args: any[]) => any;
    rotateY: (degree: any, ...args: any[]) => any;
    rotateZ: (degree: any, ...args: any[]) => any;
    draw: (renderer: any, ...args: any[]) => RenderSGNode;
    drawSphere: (radius: any, latitudeBands: any, longitudeBands: any, ...args: any[]) => any;
    drawRect: (width: any, height: any, ...args: any[]) => any;
    shader: (program: any, ...args: any[]) => ShaderSGNode;
    context: (gl: any, projectionMatrix: any) => any;
  }
  export const sg: SGUtils;
}
