/**
 * Created by Samuel Gratzl on 08.02.2016.
 * Typescript tooling/seed by Manuel Pöll on 18.3.2019
 */
import { loadResources } from 'cg-lab-framework';

import { CustomContext, CustomResourcesPaths, CustomResources } from './app/model';

import { init } from './app/init';
import { render } from './app/render';

export function main(): void {
  const resourcePaths: CustomResourcesPaths = {
    vs_phong: 'src/shader/phong.vs.glsl',
    fs_phong: 'src/shader/phong.fs.glsl',

    vs_single: 'src/shader/single.vs.glsl',
    fs_single: 'src/shader/single.fs.glsl',

    model_c3po: 'src/models/C-3PO.obj'
  }

  // load resources for
  loadResources(resourcePaths).then(function (resources: CustomResources) {
    const customContext: CustomContext = init(resources);

    //render one frame
    render(customContext, 0);
  });
}

main();
