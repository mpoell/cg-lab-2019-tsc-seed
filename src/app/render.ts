/**
 * Created by Samuel Gratzl on 08.02.2016.
 * Typescript tooling/seed by Manuel Pöll on 18.3.2019
 */
import { mat4 } from 'gl-matrix';

import { checkForWindowResize, glm, createSGContext } from 'cg-lab-framework';

import { CustomContext } from './model';

/**
 * render one frame
 */
export function render(customContext: CustomContext, timeInMilliseconds: DOMHighResTimeStamp) {
  const gl = customContext.gl;
  const camera = customContext.camera;

  checkForWindowResize(gl);

  gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
  //set background color to light gray
  gl.clearColor(0.9, 0.9, 0.9, 1.0);
  //clear the buffer
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


  const context = createSGContext(gl);
  context.projectionMatrix = mat4.perspective(mat4.create(), glm.deg2rad(30), gl.drawingBufferWidth / gl.drawingBufferHeight, 0.01, 100);

  //ReCap: what does this mean?
  context.viewMatrix = mat4.lookAt(mat4.create(), [0,1,-10], [0,0,0], [0,1,0]);

  //TASK 0-2 rotate whole scene according to the mouse rotation stored in
  //camera.rotation.x and camera.rotation.y
  context.sceneMatrix = mat4.multiply(mat4.create(),
                            <mat4>glm.rotateY(camera.rotation.x),
                            <mat4>glm.rotateX(camera.rotation.y));

  customContext.c3poTransformNode.matrix = glm.rotateY(timeInMilliseconds*-0.01);

  //TASK 4-2 enable light rotation
  customContext.light1TransformNode.matrix = glm.rotateY(timeInMilliseconds*0.05);
  //TASK 5-2 enable light rotation
  customContext.light2TransformNode.matrix = glm.rotateY(-timeInMilliseconds*0.1);

  customContext.rootNode.render(context);

  //animate
  requestAnimationFrame(render.bind(null, customContext));
}
