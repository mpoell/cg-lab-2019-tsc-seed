/**
 * Created by Samuel Gratzl on 08.02.2016.
 * Typescript tooling/seed by Manuel Pöll on 18.3.2019
 */
import { mat4 } from 'gl-matrix';

import { createContext, createProgram, glm, Model, vec3Untyped, vec4Untyped } from 'cg-lab-framework';
import { makeSphere, makeRect } from 'cg-lab-framework';
import { RenderSGNode, TransformationSGNode, ShaderSGNode } from 'cg-lab-framework';
import { LightSGNode, MaterialSGNode } from 'cg-lab-framework';

import { CustomResources, CustomContext } from './model';
import { Nullable } from './util';

/**
 * initializes OpenGL context, compile shader, and load buffers
 */
export function init(resources: CustomResources): CustomContext {
  //create a GL context
  const gl: Nullable<WebGLRenderingContext> = createContext();
  if (gl == null) {
    throw new Error('Failed to acquire WebGL Rendering Context!');
  }

  //enable depth test to let objects in front occluse objects further away
  gl.enable(gl.DEPTH_TEST);

  const customContext = createSceneGraph(gl, resources);

  initInteraction(gl.canvas, customContext);

  return customContext;
}

function createSceneGraph(gl: WebGLRenderingContext, resources: CustomResources): CustomContext {
  const rootNode = initPhongShader(gl, resources);

  const light1TransformNode = new TransformationSGNode(mat4.create(), [
    initLightNode(gl, resources, 'u_light',
      [0, 2, 2], [0, 0, 0, 1], [1, 1, 1, 1], [1, 1, 1, 1])
  ]);
  rootNode.append(light1TransformNode);

  const light2TransformNode = new TransformationSGNode(mat4.create(), [
    initLightNode(gl, resources, 'u_light2',
      [0, 2, 2], [0, 0, 0, 0], [1, 0, 0, 1], [1, 0, 0, 1])
  ]);
  rootNode.append(light2TransformNode);

  const c3poTransformNode = new TransformationSGNode(mat4.create(), [
    new TransformationSGNode(glm.translate(0,-1.5, 0),  [
      initMaterialNode(<Model>resources.model_c3po,
        [0.24725, 0.1995, 0.0745, 1], [0.75164, 0.60648, 0.22648, 1], [0.628281, 0.555802, 0.366065, 1], 0.4)
    ])
  ]);
  rootNode.append(c3poTransformNode);

  const floorTransformNode = new TransformationSGNode(glm.transform({ translate: [0,-1.5,0], rotateX: -90, scale: 3 }), [
    initMaterialNode(makeRect(2, 2),
      [0, 0, 0, 1], [0.1, 0.1, 0.1, 1], [0.5, 0.5, 0.5, 1], 0.7)
  ])
  rootNode.append(floorTransformNode);

  return <CustomContext>{
    gl: gl,
    rootNode: rootNode,
    light1TransformNode: light1TransformNode,
    light2TransformNode: light2TransformNode,
    c3poTransformNode: c3poTransformNode,
    camera: {
      rotation: { x: 0, y: 0 }
    }
  };
}

function initPhongShader(gl: WebGLRenderingContext, resources: CustomResources): ShaderSGNode {
  // Create Phong Shader for root node
  const phongShaderProgram: Nullable<WebGLProgram> = createProgram(gl, <string>resources.vs_phong, <string>resources.fs_phong);
  if (phongShaderProgram == null) {
    throw new Error('Failed to compile/link programm!');
  }
  return new ShaderSGNode(phongShaderProgram);
}

function initLightNode(
  gl: WebGLRenderingContext, resources: CustomResources, uniform: string,
  position: vec3Untyped, ambient: vec4Untyped, diffuse: vec4Untyped, specular: vec4Untyped,
): LightSGNode {
  const lightNode = new LightSGNode(position, createLightSphere(gl, resources));
  lightNode.uniform = uniform;
  lightNode.ambient = ambient;
  lightNode.diffuse = diffuse;
  lightNode.specular = specular;

  return lightNode;
}

function initMaterialNode(
  model: Model,
  ambient: vec4Untyped, diffuse: vec4Untyped, specular: vec4Untyped, shininess: number
): MaterialSGNode {
  const materialNode = new MaterialSGNode([
    new RenderSGNode(model)
  ]);
  // gold
  materialNode.ambient = ambient;
  materialNode.diffuse = diffuse;
  materialNode.specular = specular;
  materialNode.shininess = shininess;

  return materialNode;
}

function createLightSphere(gl: WebGLRenderingContext, resources: CustomResources) {
  // Create flat shader for light sphere
  const flatShaderProgram: Nullable<WebGLProgram> = createProgram(gl, <string>resources.vs_single, <string>resources.fs_single);
  if (flatShaderProgram == null) {
    throw new Error('Failed to compile/link programm!');
  }

  return new ShaderSGNode(flatShaderProgram, [
    new RenderSGNode(makeSphere(.2,10,10))
  ]);
}

function initInteraction(canvas: HTMLCanvasElement, customContext: CustomContext) {
  const mouse = {
    pos: { x : 0, y : 0},
    leftButtonDown: false
  };

  canvas.addEventListener('mousedown', function(event: MouseEvent) {
    mouse.pos = toPos(canvas, event);
    mouse.leftButtonDown = event.button === 0;
  });
  canvas.addEventListener('mousemove', function(event: MouseEvent) {
    const pos = toPos(canvas, event);
    const delta = { x : mouse.pos.x - pos.x, y: mouse.pos.y - pos.y };
    //TASK 0-1 add delta mouse to camera.rotation if the left mouse button is pressed
    if (mouse.leftButtonDown) {
      //add the relative movement of the mouse to the rotation variables
  		customContext.camera.rotation.x += delta.x;
  		customContext.camera.rotation.y += delta.y;
    }
    mouse.pos = pos;
  });
  canvas.addEventListener('mouseup', function(event: MouseEvent) {
    mouse.pos = toPos(canvas, event);
    mouse.leftButtonDown = false;
  });
  //register globally
  document.addEventListener('keypress', function(event: KeyboardEvent) {
    //https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent
    if (event.code === 'KeyR') {
      customContext.camera.rotation.x = 0;
  		customContext.camera.rotation.y = 0;
    }
  });
}

function toPos(canvas: HTMLCanvasElement, event: MouseEvent) {
  //convert to local coordinates
  const rect = canvas.getBoundingClientRect();
  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top
  };
}
