import { ResourcePaths, ResolveResources } from 'cg-lab-framework';

export interface CustomResourcesPaths extends ResourcePaths {
  vs_phong: string;
  fs_phong: string;

  vs_single: string;
  fs_single: string;

  model_c3po: string;
}

export type CustomResources = ResolveResources<CustomResourcesPaths>;
