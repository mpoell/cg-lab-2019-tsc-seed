import { SGNode, TransformationSGNode } from 'cg-lab-framework';
import { Camera } from './camera';

export interface CustomContext {
  gl: WebGLRenderingContext;
  rootNode: SGNode;
  light1TransformNode: TransformationSGNode;
  light2TransformNode: TransformationSGNode;
  c3poTransformNode: TransformationSGNode;
  camera: Camera;
}
