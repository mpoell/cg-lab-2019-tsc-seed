
export interface CameraRotation {
  x: number;
  y: number;
}

export interface Camera {
  rotation: CameraRotation;
}
