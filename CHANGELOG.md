# CG Lab 2019 Typescript Seed #

### Version 1.1.0

* Expose ES6 classes to window, thus enabling visibility in webpack bundle
* Port 'Lab 04 illumination and shading full version'
* Mark several params (with default handling) as optional (properties of TransformSettings, children constructor params of various SGNodes)
* Improve vectors/matrix typings
* Improve resource typings


## Version 1.0.0

* Initial Release with typescript and webpack support for JKU CG Lab 2019
